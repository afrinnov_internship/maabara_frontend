
import { QueryClient, QueryClientProvider } from 'react-query';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Container from './components/Container/Container';
import DetailExam from './components/Detail/DetailExam';
import ExamForm from './components/Form/ExamForm';
import ListExam from './components/ListPage/ListExam';


const queryClient = new QueryClient()

function App() {
  return (
  <QueryClientProvider client={queryClient}>
    <BrowserRouter>
        <Routes>
          <Route path="/" element={<Container />}>
              <Route path="add-exams" element={<ExamForm />}/>
              <Route path="" element={<ListExam/>}/>
              <Route path="exam/:code" element={<DetailExam/>}/>
          </Route>
        </Routes>
    </BrowserRouter>
  </QueryClientProvider>
  );
}

export default App;
