
import { Button, Form, Input,  message, Select} from 'antd';
import { ChangeEvent, FormEvent, useCallback, useState } from 'react';
import { useQuery } from 'react-query';
import { examCreate } from '../../api/ExamApi';
import { ExamFormType } from '../../types';
import { examGroupList } from '../../api/groupExamApi';
import ExamNavMenu from '../ExamNavMenu /ExamNavMenu';

const { Option } = Select;

export default function ExamForm(){
  const [loading, setLoading] = useState(false);
  const {data} = useQuery('examGroup/list', () => examGroupList())
  const [form, setForm] = useState<ExamFormType>({
    examDescription:"",
    examLabel:"",
    examMaxValue:0,
    examMinValue:0,
    examPrice:0,
    examResultUnite:"",
    examGroup:0,
  });

  const handleFormChange = useCallback((event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement >) => {
    setForm((oldForm) => ({...oldForm, [event.target.name]: event.target.value}))
  }, []);

  const handleFormSubmit = useCallback(async (event: FormEvent) => {
    
    try {
      console.log(form)
      event.preventDefault();
      setLoading(true);
      const response = await examCreate(form)
      if(response.examCode){
        message.info("exam succesfully Created")
      }
    } catch (error) {
      message.error("exam was not created check your form")
    } finally{
      setLoading(false)
    }
  },[form])

  

    return (
      <div>
        <ExamNavMenu />
      <Form onSubmitCapture={handleFormSubmit} style={{ marginLeft:'30%', marginRight:'30%', marginTop:'2%', padding:'10px', border: ' 3px solid' }}>
      <h1 style={{textAlign:'center', fontFamily: 'Times New Roman'}}>
        ADD AN EXAM
      </h1>
      
      <label>
        NAME:
      </label>
      <Form.Item style={{border: '1px solid', borderRadius:'3%', color:'black'}}>
        <Input name="examLabel" value={form.examLabel} onChange={handleFormChange} disabled={loading} />
      </Form.Item>
      <label>
        MINVALUE:
      </label>
      <Form.Item rules={[{ type: 'number'}]} style={{border: '1px solid', borderRadius:'3%', color:'black'}}>
        <Input type='number' style={{width:'100%'}} name="examMinValue" value={form.examMinValue} onChange={handleFormChange} disabled={loading}/>
      </Form.Item>
      <label>
        MAXVALUE:
      </label>
      <Form.Item rules={[{ type: 'number'}]} style={{border: '1px solid', borderRadius:'3%', color:'black'}}>
        <Input type='number' style={{width:'100%'}} name="examMaxValue" value={form.examMaxValue} onChange={handleFormChange} disabled={loading}/>
      </Form.Item>

      <label>
        PRICE:
      </label>
      <Form.Item rules={[{ type: 'number'}]} style={{border: '1px solid', borderRadius:'3%', color:'black'}}>
        <Input type='number' style={{width:'100%'}} name="examPrice" value={form.examPrice} onChange={handleFormChange} disabled={loading}/>
      </Form.Item>
      
      <label>
        ResultUnite:
      </label>
      <Form.Item style={{border: '1px solid', borderRadius:'3%', color:'black'}}>
        <Input name="examResultUnite" value={form.examResultUnite} onChange={handleFormChange} disabled={loading}/>
      </Form.Item>
      <label>
        Description:
      </label>
      <Form.Item style={{border: '1px solid', borderRadius:'3%', color:'black'}}>
        <Input.TextArea name="examDescription" value={form.examDescription} onChange={handleFormChange} disabled={loading}/>
      </Form.Item>
      <Form.Item >

      <Form.Item style={{border: '1px solid', borderRadius:'3%', color:'black'}}>
       <Select >
       {data?.map((l,index) => {
         console.log(l)
         return (<Select.Option key={index} value={l.examGroupCode}>{l.examGroupLabel}</Select.Option>)
        })}
        </Select>
      </Form.Item>
        <Button type="primary" htmlType="submit" style={{width:'100%', borderRadius:'15px', margin:'5px'}} disabled={loading} loading= {loading}>
          Submit
        </Button>
      </Form.Item>
    </Form>
    </div>
    );
}