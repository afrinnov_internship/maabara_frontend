import {
    FormOutlined,
    SearchOutlined,
    SnippetsOutlined,
    UserOutlined,
  } from '@ant-design/icons';
import { Avatar} from 'antd';
import { Menu } from 'antd';
import Layout, { Content, Footer } from 'antd/lib/layout/layout';
import Sider from 'antd/lib/layout/Sider';
import { useState } from 'react';
import { Link, Outlet, useLocation } from 'react-router-dom';

export default function SideBar() {

  const routes = [
    {path: "a", name:"Consultation", icon:<SearchOutlined style={{fontSize:17}} />},
    {path: '/', name:"Examen", icon:<FormOutlined style={{fontSize:17}} />},
    {path: "l", name:"Laboratoire", icon:<SnippetsOutlined style={{fontSize:17}} />},
    {path: "m", name:"Patient", icon:<UserOutlined style={{fontSize:17 }} />},
      
      ]

const [collapsed, setCollapsed] = useState(false);
const location = useLocation();

    return (
        <Layout style={{ minHeight: '100vh' }}>
        <Sider collapsible collapsed={collapsed} onCollapse={value => setCollapsed(value)}>
          <div className="logo" />
          <Avatar 
          size="large"
          style={{width:150, height:150, marginLeft:20, marginTop:12}}
          src="https://drive.google.com/uc?export=view&id=18rEH2oCN9Xh4uOQthDmK3yaVCxbSN5r0"></Avatar>
            <Menu
                style={{marginTop:5}}
                theme="dark"
                mode="vertical"
                defaultSelectedKeys={["/"]}
                selectedKeys={[location.pathname]}
                multiple={true}
                items={routes.map((route) => ({
                    label: (
                        <Link to={route.path} key={route.path}>                        
                            {route.icon}  {route.name}
                        </Link>
                    ),
                    key: route.path
                }))}
            />
         
        </Sider>
        <Layout className="site-layout">
          <Content style={{}}>
            <Outlet />
          </Content>
          <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
        </Layout>
      </Layout>
    );
}



