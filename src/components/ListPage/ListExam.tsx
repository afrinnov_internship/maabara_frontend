
import { Input, Table } from 'antd';
import Column from 'antd/lib/table/Column';
import { useNavigate } from "react-router-dom";
import { useQuery } from 'react-query';
import { examList } from '../../api/ExamApi';
import ExamNavMenu from '../ExamNavMenu /ExamNavMenu';

export default function ListExam(){

    const { Search } = Input;

    const onSearch = (value: string) => console.log(value);  

    const {data} = useQuery("exam/list", () => examList() );
    
    let navigate = useNavigate();

    return(
      <div>
        <ExamNavMenu />
        <div style={{marginLeft:'15%', marginRight:'15%', marginTop:'2%', }}>
            <div style={{display: 'flex', flexDirection: 'row'}}>
                <div style={{marginTop : 5}}>
                <h1 style={{fontFamily: 'Times New Roman', fontSize: 'large', marginLeft: 10}}>EXAM LIST</h1>
                </div>
                <div style={{marginLeft: 'auto' , marginTop:4,}}>
                <Search placeholder="input search text" allowClear onSearch={onSearch} style={{ width: 200}} />
                </div>
            </div>
            <div>
            <Table
             dataSource={data}
             onRow = {(record, rowIndex) => {
              return {
                onClick: event => {navigate(`/exam/${record.examCode}`)},}}}
          >
    {/* <ColumnGroup title="Name"> */}
      examCode
    <Column title=" NAME" dataIndex="examLabel" key="examLabel" />
    {/*</ColumnGroup>*/}
    <Column title="PRIX" dataIndex="examPrice" key="examPrice" />
    <Column title="Unite de Mesure" dataIndex="examResultUnite" key="examResultUnite" />
  </Table>
            </div>
        </div>
        </div>
    );
}