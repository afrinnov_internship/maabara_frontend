import { Button} from "antd";
import { useQuery } from "react-query";
import { examDetails } from "../../api/ExamApi";

export default function DetailExam(){

    const {data} = useQuery("/exam/:code", () => examDetails());

    return(
    <div style={{marginLeft:'18%', marginRight:'18%', marginTop: '3%'}}>
        <div style={{textAlign:'center', fontFamily:'Time New Roman'}}><h1>EXAM DETAIL </h1></div>
        <br/>
        <div style={{display: 'flex', flexDirection:'row'}}>
        <div style={{fontFamily:'Time New Roman', display: 'flex', flexDirection:'column' }}>
            
            <h3>NAME: {data?.examLabel}</h3>
            <br/>
            <h3>MINVALUE: {data?.examMinValue} {data?.examResultUnite}</h3>
            <br/>
            <h3>MAXVALUE: {data?.examMaxValue} {data?.examResultUnite}</h3>
            <br/>
            <h3>PRICE: {data?.examPrice}FCFA</h3>
            <br/>
            <div style={{display: 'flex', flexDirection:'row' }}>
            <Button type="primary" style={{width:80, marginRight:2 }}> Edit</Button>
            <Button type="primary" danger style={{width:80, marginRight:2 }}> Delete </Button>
            </div>
        </div>
    <div style={{ marginLeft:'auto', marginBottom:'auto',minHeight:3 ,maxWidth:300, minWidth:275, border:'1px solid', borderRadius:'9px'}}>
        <h3 style={{marginLeft:10 }}>
        DESCRIPTION: 
        <h5>
        {data?.examDescription}
        </h5>
        </h3>
        
        </div>
        </div>
        <div>

        </div>
    </div>)
}