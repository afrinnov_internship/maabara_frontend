import { PlusCircleOutlined, UnorderedListOutlined } from "@ant-design/icons";
import { Menu } from "antd";
import { Header } from "antd/lib/layout/layout";
import { Link, useLocation } from "react-router-dom";

const routes = [
    {path: "/add-exams", name: "Add An Exam", icon:<PlusCircleOutlined />},
    {path: "/", name: "Exam List", icon:<UnorderedListOutlined />},
]

export default function ExamNavMenu() {
    const location = useLocation();
    return (
        <Header>
            <div className="logo" />
            <Menu
                theme="dark"
                mode="horizontal"
                defaultSelectedKeys={["/list-exam"]}
                selectedKeys={[location.pathname]}
                multiple={false}
                items={routes.map((route) => ({
                    label: (
                        <Link to={route.path} key={route.path}>                        
                            {route.icon}  {route.name}
                        </Link>
                    ),
                    key: route.path
                }))}
            />
        </Header>
    )
}