import { Layout } from "antd";
import SideBar from "../SideBar/SideBar";


export default function Container(){
    return(
        <Layout>
            <SideBar />
        </Layout>
    );

}