export type ExamFormType = {
    examMinValue: number;
    examMaxValue: number;
    examLabel: string;
    examDescription: string;
    examResultUnite: string;
    examPrice: number;
    examGroup: number;
}

export type ExamListType = {
    examCode: string,
    examLabel: string,
    examPrice: number,
    examResultUnite: String
}

export type ExamDetailType = {
    examCode: String;
    examMinValue: number;
    examMaxValue: number;
    examLabel: string;
    examDescription: string;
    examResultUnite: string;
    examPrice: number;
}

export type ExamGroupList={
    examGroupLabel: string;
    examGroupCode: string;
    deleted:boolean;
}

export type ResponseType <T> ={
    data: T;
}