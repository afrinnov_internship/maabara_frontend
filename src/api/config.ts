import axios from "axios";
import MockAdapter from "axios-mock-adapter";

const apiConfig = {
    url: process.env.REACT_APP_API_URL,
    timeout: 50000,
    headers: {
        'Content-Type': 'application/json',
        accept: 'application/json',
    },
};

const apiAxiosInstance = axios.create({
    baseURL: apiConfig.url,
    headers: apiConfig.headers,
    timeout: apiConfig.timeout,
});

const axiosMockInstance = axios.create();

export const mock = new MockAdapter(axiosMockInstance, {delayResponse: 4000});
export default process.env.REACT_APP_MOCK == "true" ? axiosMockInstance: apiAxiosInstance;

