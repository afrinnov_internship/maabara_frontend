import { ExamDetailType, ExamFormType, ExamListType, ResponseType} from "../types";
import apiAxiosInstance from "./config"

export const examList = async (): Promise<ExamListType[]> => {
    const response = await apiAxiosInstance.get<ResponseType<ExamListType[]>>("exam/list");
    return response.data.data
}

export const examCreate = async (form: ExamFormType): Promise<ExamDetailType> => {
    const response = await apiAxiosInstance.post<ResponseType<ExamDetailType>>("exam/add", form);
    return response.data.data
}

export const examDetails = async (): Promise<ExamDetailType> => {
    const response = await apiAxiosInstance.get<ResponseType<ExamDetailType>>("/exam/details/:code");
    return response.data.data
}
