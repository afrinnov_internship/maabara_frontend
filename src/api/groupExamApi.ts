import { ExamDetailType, ExamFormType, ExamGroupList, ExamListType, ResponseType} from "../types";
import apiAxiosInstance from "./config"

export const examGroupList = async (): Promise<ExamGroupList[]> => {
    const response = await apiAxiosInstance.get<ResponseType<ExamGroupList[]>>("examGroup/list");
    return response.data.data
}