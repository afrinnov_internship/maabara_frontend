import { mock } from "../api/config"
import { ExamDetailType, ExamFormType, ExamListType } from "../types";


let examsFakeData: ExamDetailType[] = [
    { 
        examCode:"",
        examMinValue: 0,
        examMaxValue: 0,
        examLabel: "",
        examDescription: "",
        examResultUnite: "",
        examPrice: 0
    }
]

const examsDetailFakeData = 
    { 
        examCode:"1",
        examMinValue: 1,
        examMaxValue: 5,
        examLabel: "scanner",
        examDescription: "Le scanner combine des rayons X (comme la radiographie) à des supports informatiques, ce qui permet de reconstruire des images dans les différentes dimensions : on peut ainsi obtenir des vues en coupe longitudinale ou horizontale.",
        examResultUnite: "bps",
        examPrice: 50000
    }


let examsListData: ExamListType[] = [
    {
        examCode:"1",
        examLabel:"scanner",
        examPrice:45000,
        examResultUnite:"pbs"
     },{
        examCode:"2",
        examLabel:"goutte epaise",
        examPrice:50000,
        examResultUnite:"gtr"
     },{
        examCode:"3",
        examLabel:"test3",
        examPrice:550000,
        examResultUnite:"hft"
     },{
        examCode:"4",
        examLabel:"test4",
        examPrice:35000,
        examResultUnite:"trf"
     },{
        examCode:"5",
        examLabel:"test5",
        examPrice:30000,
        examResultUnite:"frt"
     },{
        examCode:"6",
        examLabel:"test6",
        examPrice:25000,
        examResultUnite:"jdk"
     },{
        examCode:"7",
        examLabel:"test7",
        examPrice:20000,
        examResultUnite:"qtr"
     },{
        examCode:"8",
        examLabel:"test8",
        examPrice:15000,
        examResultUnite:"htr"
     },{
        examCode:"9",
        examLabel:"test9",
        examPrice:10000,
        examResultUnite:"bps"
     },{
        examCode:"10",
        examLabel:"test10",
        examPrice:5500,
        examResultUnite:"bpr"
     },{
        examCode:"11",
        examLabel:"test11",
        examPrice:6500,
        examResultUnite:"tre"
     },{
        examCode:"12",
        examLabel:"test12",
        examPrice:96500,
        examResultUnite:"trz"
     },{
        examCode:"13",
        examLabel:"test13",
        examPrice:100000,
        examResultUnite:"hdr"
     },{
        examCode:"14",
        examLabel:"test14",
        examPrice:57500,
        examResultUnite:"pbr"
     },{
        examCode:"15",
        examLabel:"test15",
        examPrice:96500,
        examResultUnite:"bps"
     }
]

mock.onGet("/exam/list").reply(200,{
    data: examsListData,
});

mock.onGet("/exam/details/:code").reply(200, {
   data: examsDetailFakeData,
});


mock.onPost("exam/add").reply<ExamDetailType>((config) => {
   const exam: ExamFormType = config.data;
   const examDetail: ExamDetailType = {
    examCode: new Date().getTime().toString(),
    examLabel: exam.examLabel,
    examDescription: exam.examDescription,
    examMaxValue:exam.examMaxValue,
    examMinValue:exam.examMinValue,
    examPrice:exam.examPrice,
    examResultUnite:exam.examResultUnite,
   };
   
   examsFakeData = [...examsFakeData, examDetail]

 return [200, {data:examDetail}]
})

