import { mock } from "../api/config";
import { ExamGroupList } from "../types";

let examsGroupFakeData: ExamGroupList[] = [
    { 
        examGroupLabel:"riverse",
        examGroupCode: "7",
        deleted: false
    },
    { 
        examGroupLabel:"scan",
        examGroupCode:"6",
        deleted: false
    },
    { 
        examGroupLabel:"palu",
        examGroupCode:"5",
        deleted: false
    },
    { 
        examGroupLabel:"sanguin",
        examGroupCode:"4",
        deleted: false
    },
    { 
        examGroupLabel:"nerf",
        examGroupCode:"3",
        deleted: false
    },
    { 
        examGroupLabel:"musculaire",
        examGroupCode:"2",
        deleted: false
    },
    { 
        examGroupLabel:"kalo",
        examGroupCode:"1",
        deleted: false
    },
]

mock.onGet("examGroup/list").reply(200,{
    data: examsGroupFakeData,
});